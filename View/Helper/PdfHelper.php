<?php
/**
 * Copyright 2013, Alexandre Breteau (http://seldszar.fr)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * Copyright 2013, Alexandre Breteau (http://seldszar.fr)
 * @license MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
App::uses('AppHelper', 'View/Helper');

class PdfHelper extends AppHelper {

/**
 * Additional helpers
 *
 * @var array
 */
	public $helpers = array('Html');

/**
 * A barcode
 *
 * @param mixed $value Barcode value
 * @param string $type Barcode type
 * @param array $settings Additional settings for this barcode
 * @return string Barcode html
 */
	public function barcode($value, $type, $settings = array()) {
		$settings = Hash::merge(array(
			'value' => $value,
			'type' => $type
		), $settings);
		return $this->Html->tag('barcode', false, $settings);
	}

/**
 * A QR-Code
 *
 * @param mixed $value QR-Code value
 * @param string $ec QR-Code error correction level
 * @param array $settings Additional settings for this QR-Code
 * @return string QR-Code html
 */
	public function qrcode($value, $ec = 'H', $settings = array()) {
		$settings = Hash::merge(array(
			'value' => $value,
			'ec' => $ec
		), $settings);
		return $this->Html->tag('qrcode', false, $settings);
	}

/**
 * A bookmark
 *
 * @param string $title
 * @param integer $level
 * @return string Bookmark html
 */
	public function bookmark($title, $level) {
		return $this->Html->tag('bookmark', false, array(
			'title' => $title,
			'level' => $level
		));
	}

}
