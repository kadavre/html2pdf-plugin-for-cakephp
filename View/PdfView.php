<?php
/**
 * Copyright 2013, Alexandre Breteau (http://seldszar.fr)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * Copyright 2013, Alexandre Breteau (http://seldszar.fr)
 * @license MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
App::uses('View', 'View');
App::uses('PdfRegistry', 'HTML2PDF.Utility');

class PdfView extends View {

/**
 * The subdirectory. PDF views are always in pdf.
 *
 * @var string
 */
	public $subDir = 'pdf';

/**
 * Constructor
 *
 * @param Controller $controller
 */
	public function __construct(Controller $controller = null) {
		parent::__construct($controller);
		if (isset($controller->response) && $controller->response instanceof CakeResponse) {
			$controller->response->type('pdf');
			$this->layoutPath = 'pdf';
		}
	}

	public function render($view = null, $layout = null) {
		$settings = array();
		if (isset($this->viewVars['_pdf'])) {
			$settings = Hash::merge($settings, $this->viewVars['_pdf']);
			unset($this->viewVars['_pdf']);
		}
		if ($view !== false && $this->_getViewFileName($view)) {
			$content = parent::render($view, $layout);
			$Pdf = PdfRegistry::init($settings);
			$Pdf->WriteHTML($content);
			return $Pdf->Output();
		}
	}

}
